# vpn

An elegant command-line interface for [AzireVPN](https://www.azirevpn.com/)’s WireGuard service for the [Fish shell](https://fishshell.com/).

![Screenshot](screenshot.png)

## Install

### Fisher

[Fisher](https://github.com/jorgebucaran/fisher) can be used to install the plugin directly from this repository.

```console
fisher install https://gitlab.com/mirdono/vpn
```

Alternatively, clone this repository and use Fisher to install from the local directory.

```console
fisher install ~/path/to/cloned/repository/vpn
```

### Manual

Clone this repository and copy the files in the `conf.d` and `functions` folders to the respective folders in your `$__fish_config_dir`.

## Usage

### Connect to the default server

```sh
❯ vpn -u
```

Sample output:

```
 📡 Connecting to AzireVPN in Netherlands (Amsterdam)...
 ✅ Done.
```

### Connect to a specific server

```sh
❯ vpn -ude-ber
```

Sample output:

```
 ✅ You are already connected to AzireVPN in Netherlands (Amsterdam) via WireGuard.
 👋 Disconnecting from AzireVPN in Netherlands (Amsterdam)...
 ✅ Done.
 📡 Connecting to AzireVPN in Germany (Berlin)...
 ✅ Done.
```

### Get list of currently supported servers

```sh
❯ vpn -l
```

Sample output:

```
 Currently supported locations and their corresponding argument:

 Location                     Argument
 ----------------------       -----------
 Canada (Toronto)             canada
 Switzerland (Zurich)         switzerland
 Germany (Berlin)             germany-ber
 Germany (Frankfurt)          germany-fra
 Denmark (Copenhagen)         cph
 Spain (Madrid)               spain-mad
 Spain (Málaga)               spain-mal
 Finland (Helsinki)           finland
 France (Paris)               france
 UK (London)                  uk
 Hong Kong (Hong Kong)        hong-kong
 Italy (Milan)                italy
 Netherlands (Amsterdam)      netherlands
 Norway (Oslo)                norway
 Romania (Bucharest)          romania
 Sweden (Stockholm)           sweden
 Sweden (Gothenburg)          sweden-got
 Singapore (Singapore)        singapore
 Thailand (Rawai)             thailand
 USA (Chicago)                us-chi
 USA (Dalas)                  us-dal
 USA (Los Angeles)            us-lax
 USA (Miami)                  us-mia
 USA (New York)               us-nyc
 USA (Seatle)                 us-sea
```

### Disconnect

```sh
❯ vpn -d
```

Sample output:

```
 👋 Disconnecting from AzireVPN in Germany (Berlin)...
 ✅ Done.
```

### Get connection status

```sh
❯ vpn -s
```

Sample output:

```
 ✅ You are connected to AzireVPN in Netherlands (Amsterdam) via WireGuard.
```

```
 ❌ You are not connected to AzireVPN via WireGuard.
```

## Acknowledgements

This is a Fish shell alternative inspired by [Aral Balkan's original application](https://source.small-tech.org/aral/vpn).

## Disclaimer

I am not affiliated with AzireVPN and this plugin is not endorsed by AzireVPN in any way.

## License

[GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.html)

## Repositories

- Sourcehut: https://git.sr.ht/~onodrim/vpn
- Mirror on Gitlab: https://gitlab.com/mirdono/vpn
- Mirror on Codeberg https://codeberg.org/onodrim/vpn
