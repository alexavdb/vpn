complete vpn -x

complete vpn -x -s v -l version -d "Display version information"
complete vpn -x -s h -l help -d "Display help message"
complete vpn -x -s l -l list -d "Display a list of supported locations and location codes"
complete vpn -x -s s -l status -d "Display current connection status"
complete vpn -x -s u -l up -d "Connect to the specified location, or the default location if none is provided"
complete vpn -x -s d -l down -d "Disconnect the current connection, if any"
